package ictgradschool.industry.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ExerciseTwo_Array_Of_String {


    public static void main(String[] args) {
        (new ExerciseTwo_Array_Of_String()).getMyList();
    }


    public void getMyList() {
        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        ArrayList <String> myList = new ArrayList <String>();
        Collections.addAll(myList, array);

        // convert all items in myList to lowercase using a for loop
        // TODO

        for (int i = 0; i < myList.size(); i++) {
            String str = myList.get(i);
            myList.set(i, str.toLowerCase());
        }

        System.out.println(myList);
        myList.clear();
        Collections.addAll(myList, array);

        // convert all items in myList to lowercase using an iterator
        Iterator <String> myIterator = myList.iterator();
        int i = 0;
        while (myIterator.hasNext()) {
            String element = myIterator.next().toLowerCase();
            myList.set(i, element);
            i++;
        }

        System.out.println(myList);
        myList.clear();
        Collections.addAll(myList, array);

        // convert all items in myList to lowercase using an enhanced for loop
        // TODO
        i = 0;
        for (String element : myList) {
            String lowercase = element.toLowerCase();
            myList.set(i, lowercase);
            i++;
        }
        System.out.println(myList);


        //System.out.println(myList.get(0)+" "+myList.get(1)+" "+myList.get(2)+" "+myList.get(3)+" "+myList.get(4)+" "+myList.get(5)+" "+myList.get(6));
    }
}
